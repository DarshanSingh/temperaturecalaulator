//
//  ViewController.swift
//  Temprature
//
//  Created by AlienBrainz_mac4 on 05/12/18.
//  Copyright © 2018 Allien Brainz Software Pvt. Ltd. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var textTemp: UITextField!
    @IBOutlet weak var tempImg: UIImageView!
    @IBOutlet weak var segTemp: UISegmentedControl!
    @IBOutlet weak var tempLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }


    @IBAction func calculateTemp(_ sender: Any) {
        let temp = Int(textTemp.text!)!
        if segTemp.selectedSegmentIndex == 0{
            let temp1 = (temp - 32) * 5/9
            tempLabel.text = "\(temp1) Celsius"
            if temp1 > 36{
                tempImg.image = #imageLiteral(resourceName: "thermometer (4)")
            }else{
                tempImg.image = #imageLiteral(resourceName: "thermometer")
            }
            
        }else if segTemp.selectedSegmentIndex == 1{
            let temp1 = (temp * 9/5) + 32
            tempLabel.text = "\(temp1) Fahernheit"
            if temp1 > 100{
                tempImg.image = #imageLiteral(resourceName: "thermometer (4)")
            }else{
                tempImg.image = #imageLiteral(resourceName: "thermometer")
            }
        }
    }
}

